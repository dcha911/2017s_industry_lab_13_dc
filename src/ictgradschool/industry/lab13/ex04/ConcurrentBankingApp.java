package ictgradschool.industry.lab13.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by dcha911 on 9/01/2018.
 */
public class ConcurrentBankingApp {

        List<Transaction> rawTransactions = TransactionGenerator.readDataFile();

        BankAccount account = new BankAccount();

        BlockingQueue<Transaction> transactionBlockingQueue = new ArrayBlockingQueue<Transaction>(10);

        public void addRawTransactions () {
                for (Transaction t : rawTransactions) {
                    try {
                        transactionBlockingQueue.put(t);
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                }
        }

    public void consumeMethod() {
        while (true) {
            Transaction transaction;
            try {
                transaction = transactionBlockingQueue.take();
            }catch (InterruptedException e) {
                System.out.println(e.getMessage());
                break;
            }

            switch (transaction._type) {
                    case Deposit:
                        account.deposit(transaction._amountInCents);
                        break;
                    case Withdraw:
                        account.withdraw(transaction._amountInCents);
                        break;
            }
        }
    }

    public void start() {

            Thread consumer1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    consumeMethod();
                }
            });

        Thread consumer2 = new Thread(new Runnable() {
            @Override
            public void run() {
                consumeMethod();
            }
        });

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                addRawTransactions();
            }
        });

        producer.start();
        consumer1.start();
        consumer2.start();

        try {
            producer.join();

            while(transactionBlockingQueue.size() > 0) {
                Thread.sleep(100);
                System.out.println(transactionBlockingQueue.size());
            }

            consumer1.interrupt();
            consumer2.interrupt();

            consumer1.join();
            consumer2.join();

        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("Final balance: " + account.getFormattedBalance());

    }

    public static void main(String[] args) {
        ConcurrentBankingApp cApp = new ConcurrentBankingApp();
        cApp.start();
    }
}
