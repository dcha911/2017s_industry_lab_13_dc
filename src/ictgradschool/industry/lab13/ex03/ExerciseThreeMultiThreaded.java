package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */

    int noThreads = 4;
    double total = 0;

    @Override
    protected double estimatePI(long numSamples) {
        // TODO Implement this.

        List< Thread > threads = new ArrayList<>();

        for (int i = 0; i < noThreads; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    ThreadLocalRandom tlr = ThreadLocalRandom.current();

                    long numInsideCircle = 0;

                    for (int j = 0; j < numSamples / noThreads; j++) {

                        double x = tlr.nextDouble();
                        double y = tlr.nextDouble();

                        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                            numInsideCircle++;
                        }

                    }

                    double estimatedPi = 4.0 * (double) numInsideCircle / (double) numSamples;
                    System.out.println(Thread.currentThread().getName() + " " + estimatedPi);
                    total += estimatedPi;
                }
                });

                threads.add(thread);
                thread.start();
            }

        for (Thread t: threads) {
            try{
                t.join();
            }catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
        return total;
    }

    /** Program entry point. */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
